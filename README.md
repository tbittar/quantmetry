# Contenu du dépôt

Ce dépôt contient les fichiers du test technique de Quantmetry:
+ `rapport.pdf` est le rapport décrivant la méthodologie employée.
+ `data_v1.0 (3).csv` est le jeu de données utilisé.
+ `test_quantmetry.ipynb` est le notebook contenant le code développé pour le test. Ce notebook s'exécute avec Python 3.7 et doit se trouver dans le même dossier que le jeu de données `data_v1.0 (3).csv`. Les commentaires présents dans le notebook sont purement descriptifs, les analyses des résultats obtenus se trouvent dans le rapport.